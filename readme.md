#About
_oars-m-urls_ is a simple command line tool for bulk generating short urls
at a [YOURLS](http://yourls.org/) service.

It reads a csv file you provide it, generates short urls 
using the [YOURLS](http://yourls.org/) API, and then outputs a csv 
file containing the original url/s and the new short url/s.

# Installation

1. Extract the zip or tar to your machine
1. Optionally, add the `bin` directory to your path 

```
oars-m-urls -help
```


# Usage

Create a csv file called `longurls.csv` with data in the following format:

```
<your_reference>,<your_long_url>
```

E.g.

```
link_01,https://www.mylongdomain.com/asfdasdfasdfasdf
```

Then run `oars-m-urls` with this file as the input as follows;

```
>: oars-m-urls -i=longurls.csv -o=myshorturls.csv -title=Survey01 -u=username -p=password
```

You can also use the API Key associated with your YOURLS user account, instead of a username and password, like this:

```
>: oars-m-urls -i=longurls.txt -o=path/to/outputshorturls.txt -title=Survey01 -k=myapikey
```

To find your API key, login in to your YOURLS service, navigate to admin/tools.php, and see the section titled 
_Secure passwordless API call_


On successful completion, _oars-m-urls_ will create the file at 'path/to/outputshorturls.txt' containing the following:

```
<your_reference>,<your_long_url>,<new_short_url>
```

The '-t' argument (Title) is optional and assigns a label for short urls that helps when searching for 
urls in the YOURLS admin panel. For example you might pass a different title for each run
so you can find all urls for a particular run at a later time.


# Adding a CA Certificate to the Java keystore
If the [YOURLS](http://yourls.org/) service you are pointing at is secured with an x.509 cert that your Java installation doesn't trust, the tool will fail with an error like this:
 
>unable to find valid certification path to requested target 

You need to tell Java to explicitly trust the certificate securing the service.

For example, Java doesn't trust the [Start SSL](http://www.startssl.com) Certificate Authority by default. To fix this, assuming you _do_ trust them, follow these steps to add their CA cert to Java's list of trusted certs (Mac).

1. Open Terminal
1. cd ~
1. curl http://www.startssl.com/certs/ca.crt > startsslca.crt
1. cd /path/to/java/installation
1. sudo keytool -import -keystore ./jre/lib/security/cacerts -file ~/startsslca.crt

When asked for the keystore password, enter your own if you changed it, or the password “changeit”, which is the default. When asked to trust this certificate, enter ‘yes’. Upon success, you’ll see an message “Certificate was added to keystore”.

Now when you run the tool it should work.

Note: To get the absolute path to your current java installation run this script (Mac):

```
/user/libexec/java_home
```