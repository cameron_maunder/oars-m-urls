package com.runfb;

import org.junit.Test;

import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;

import static org.junit.Assert.*;

/**
 * Created by cam on 20/11/2016.
 */
public class OarsMUrlsTest {
    @Test
    public void main() throws Exception {
        URL url = getClass().getClassLoader().getResource("test_valid.csv");
        assertNotNull(url);
        Path f = Paths.get(url.getPath());

        Date start = new Date(), end;

        if (f.toFile().exists()) {
            // refactoring required to make this testable against a mock service.
//            OarsMUrls.main(new String[]{"-i=" + f.toString(), "-o=myoutput.csv", "-k=4968a316c9", "-t=test", "-s=https://runfb.com"});
        }
        end = new Date();

        System.out.println("Execution time: " + String.valueOf(end.getTime() - start.getTime()));
    }
}