package com.runfb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.commons.cli.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.validator.routines.UrlValidator;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class OarsMUrls {
    private static Options OPTIONS;
    private static String DEFAULT_OUTPUT_FILENAME = "output.csv";
    private static String HOST_ARG,  USERNAME_ARG = "", PASSWORD_ARG = "", API_KEY_ARG = "", TITLE_ARG = "";
    private static Path  OUTPUT_PATH, INPUT_PATH;

    public static void main(String[] args){

        configureCommandLineOptions();

        // Try to load the command line options
        CommandLineParser parser = new DefaultParser();
        CommandLine commandLine;
        try {
            commandLine = parser.parse(OPTIONS, args);
            loadCommandLineArguments(commandLine);
        } catch (ParseException pe) {
            pe.printStackTrace();
            printHelpAndDie();
            return;
        }

        // Try to open the csv input file for parsing
        CSVParser csvParser;
        try {
            csvParser = new CSVParser(new FileReader(INPUT_PATH.toFile()), CSVFormat.EXCEL);
        } catch (IOException e) {
            e.printStackTrace();
            printHelpAndDie();
            return;
        }


        // Try to create a csv file to write the output to
        CSVPrinter csvPrinter;
        try {
            csvPrinter = new CSVPrinter(new FileWriter(OUTPUT_PATH.toFile()), CSVFormat.EXCEL);

        } catch (IOException e) {
            e.printStackTrace();
            printHelpAndDie();
            return;
        }

        // now process the csv records
        for (CSVRecord record: csvParser){
            try {
                if(isValidRecord(record)) {
                    processCSVRecord( record, csvPrinter );
                } else {
                    recordError (record, csvPrinter );
                }

            } catch (IOException e) {
                System.err.println("Error processing record no: " + record.getRecordNumber());
                e.printStackTrace();
            }
        }

        // Try to close the printer...
        try {
            csvPrinter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // ...and the parser
        try {
            csvParser.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void recordError(CSVRecord record, CSVPrinter csvPrinter) throws IOException {
        List<String> outputValues = new ArrayList<>();
        for (String value: record){
            outputValues.add(value);
        }
        outputValues.add("Error: Invalid record format.");
        csvPrinter.printRecord(outputValues);
    }

    /**
     * records must be two fields and the second must be a valid http/s url
     *
     * @param record
     * @return
     */
    private static boolean isValidRecord(CSVRecord record) {
        UrlValidator validator = new UrlValidator(new String[]{"http","https"});
        if(record.size() == 2){
            return validator.isValid(record.get(1));
        }
        return false;
    }

    private static void configureCommandLineOptions() {
        OPTIONS = new Options();
        OPTIONS.addOption(new Option("s", "service",true,"Required. The host of the yourls service to use. E.g. https://runfb.com"));
        OPTIONS.addOption(new Option("i", "input",true,"The path to a csv file in the format '<id>,<long_url>'"));
        OPTIONS.addOption(new Option("o", "output",true,"Where to write the output csv file containing shortened urls"));
        OPTIONS.addOption(new Option("u", "username",true,"Username"));
        OPTIONS.addOption(new Option("p", "password",true,"Password"));
        OPTIONS.addOption(new Option("k", "apikey",true,"API Key"));
        OPTIONS.addOption(new Option("t", "title",true,"Each short url record will have this value as its title."));
        OPTIONS.addOption(new Option("h", "help",false,"Show Help"));
    }

    private static void loadCommandLineArguments(CommandLine commandLine) {
        HOST_ARG = commandLine.getOptionValue("s");
        if(HOST_ARG.isEmpty()){
            System.out.println("Error, host must be supplied");
            printHelpAndDie();
        }

        String inputCsvFile = commandLine.getOptionValue("i");

        // Input file is mandatory
        if(inputCsvFile != null){
            INPUT_PATH = Paths.get(commandLine.getOptionValue("i"));
            if (!INPUT_PATH.toFile().exists()) {
                System.out.println("Error. Input file: " + INPUT_PATH + " doesn't exist.\n\n");
                printHelpAndDie();
            }
        } else {
            printHelpAndDie();
        }

        OUTPUT_PATH = commandLine.hasOption('o') ? Paths.get(commandLine.getOptionValue("o")) : Paths.get(DEFAULT_OUTPUT_FILENAME);

        API_KEY_ARG = commandLine.hasOption('k') ? commandLine.getOptionValue('k') : "";

        if(API_KEY_ARG.isEmpty()) {
            if(commandLine.hasOption('u') && commandLine.hasOption('p')){
                USERNAME_ARG = commandLine.getOptionValue('u');
                PASSWORD_ARG = commandLine.getOptionValue('p');
            } else {
                System.out.println("You must provide either username and password, or your API key.");
                printHelpAndDie();
            }
        }

        TITLE_ARG = commandLine.hasOption('t') ? commandLine.getOptionValue('t') : "";
    }

    private static void printHelpAndDie() {
            HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp("oars-m-urls", OPTIONS);
            System.exit(1);
    }

    private static void processCSVRecord(CSVRecord record, CSVPrinter csvPrinter) throws IOException {
        List<String> outputValues = new ArrayList<>();
        for (String value: record){
            outputValues.add(value);
        }
        String shortUrl = generateShortUrl(record.get(1));
        outputValues.add(shortUrl);
        csvPrinter.printRecord(outputValues);
    }


    private static String generateShortUrl(String longUrl) {
        String shortUrl = "";
        CloseableHttpClient httpclient = HttpClients.createDefault();

        HttpPost post = new HttpPost(HOST_ARG + "/yourls-api.php");
        List<NameValuePair> nameValuePairs = new ArrayList<>();
        if(!API_KEY_ARG.isEmpty()){
            nameValuePairs.add(new BasicNameValuePair("signature", API_KEY_ARG));
        } else {
            nameValuePairs.add(new BasicNameValuePair("username", USERNAME_ARG));
            nameValuePairs.add(new BasicNameValuePair("password", PASSWORD_ARG));
        }
        nameValuePairs.add(new BasicNameValuePair("title", TITLE_ARG));
        nameValuePairs.add(new BasicNameValuePair("action", "shorturl"));
        nameValuePairs.add(new BasicNameValuePair("format", "json"));
        nameValuePairs.add(new BasicNameValuePair("url", longUrl));

        try {
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            CloseableHttpResponse response = httpclient.execute(post);

            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("failed http error code: " + response.getStatusLine().getStatusCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
            Gson gson = new GsonBuilder().create();
            YourlsJson json = gson.fromJson(br, YourlsJson.class);

            if(json.statusCode == 200){
                if("success".equals(json.status)) {
                    shortUrl = json.shorturl.toString();
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return shortUrl;
    }
}
